.templates_sha: &templates_sha e195d80f35b45cc73668be3767b923fd76c70ed5

include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'
  - project: 'freedesktop/ci-templates'     # the project to include from
    ref: *templates_sha                 # git ref of that project
    file: '/templates/fedora.yml'       # the actual file to include
  - project: 'freedesktop/ci-templates'
    ref: *templates_sha
    file: '/templates/ubuntu.yml'

# From https://gitlab.freedesktop.org/freedesktop/ci-templates/-/blob/master/src/gitlab-ci.tmpl
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: $CI_COMMIT_BRANCH

variables:
  BUILD_IMAGE_REGISTRY: registry.freedesktop.org/cangjie/build-essential
  FEDORA_PACKAGES: >
    clang-tools-extra
    cppcheck
    gcovr
    git
    lcov
    meson
    pip
    pkgconf
    sqlite-devel
  UBUNTU_PACKAGES: >
    gcovr
    git
    lcov
    libsqlite3-dev
    meson
    python3-pip
    pkgconf
  FDO_UPSTREAM_REPO: cangjie/libcangjie

  FEDORA_TAG: "2024-11-06.0"
  UBUNTU_TAG: "2024-11-06.0"

  MESON_BUILDDIR: "builddir"
  NINJA_ARGS: ""
  MESON_ARGS: "-Db_coverage=True --prefix=/usr"
  MESON_TEST_ARGS: ""

.policy:
  retry:
    max: 2
    when:
      - 'runner_system_failure'
      - 'stuck_or_timeout_failure'
      - 'scheduler_failure'
      - 'api_failure'
  interruptible: true

.default_fedora_variables:
  variables:
    FDO_DISTRIBUTION_PACKAGES: $FEDORA_PACKAGES
    FDO_DISTRIBUTION_TAG: $FEDORA_TAG

.default_ubuntu_variables:
  variables:
    FDO_DISTRIBUTION_PACKAGES: $UBUNTU_PACKAGES
    FDO_DISTRIBUTION_TAG: $UBUNTU_TAG

.default_artifacts:
  artifacts:
    paths:
      - builddir/meson-dist/libcangjie-*.tar.xz
      - builddir/meson-logs/coverage*
    expire_in: "1 week"

.default_tests:
  stage: test
  script:
    - meson setup ${MESON_ARGS} ${MESON_BUILDDIR}
    - meson dist -C ${MESON_BUILDDIR}
    - meson test -C ${MESON_BUILDDIR}
    - ninja coverage-html -C ${MESON_BUILDDIR}
  coverage: '/lines\.+:\s(\d+\.\d+\%)\s+/'

.default_code_quality:
  extends:
    - .fdo.distribution-image@fedora
    - .default_fedora_variables
  variables:
    FDO_DISTRIBUTION_VERSION: '41'
  needs:
    - prep-fedora-latest

stages:
  - prep
  - health
  - test
  - visualize

# Health check: cppcheck
cppcheck:
  extends:
    - .default_code_quality
  stage: 'health'
  before_script:
    - meson setup ${MESON_ARGS} ${MESON_BUILDDIR}
  script:
    # Show the report in plain text format and return proper exit code
    - ninja cppcheck -C ${MESON_BUILDDIR}
    # Generate XML report without showing anything on the console
    - ninja cppcheck:report -C ${MESON_BUILDDIR}
  artifacts:
    when: on_failure
    paths:
      - builddir/meson-logs/cppcheck.checkers.txt
      - builddir/meson-logs/cppcheck-xml.checkers.txt
      - builddir/meson-logs/cppcheck.xml
    expire_in: "1 week"

# Health check: clang-tidy
clang-tidy:
  extends:
    - .default_code_quality
  stage: 'health'
  before_script:
    - meson setup ${MESON_ARGS} ${MESON_BUILDDIR}
  script:
    - clang-tidy -p ./builddir --use-color ./builddir/src/*.c ./builddir/src/*.h ./builddir/tests/*.c ./builddir/tests/*.h
  allow_failure: true
  artifacts:
    when: on_failure
    paths:
      - builddir/meson-logs/clang-tidy.txt
    expire_in: "1 week"

# Health check: clang-format
clang-format:
  extends:
    - .default_code_quality
  stage: 'health'
  before_script:
    - mkdir -p ./clang-format/xml
    - mkdir -p ./clang-format/xml/src
    - mkdir -p ./clang-format/xml/tests
  script:
    - |
      # Checks clang-format change suggestions

      # Check if replacements are suggested by clang-format
      FAIL=0
      for FN in $(find ./src ./tests -name '*.c' -o -name '*.h' | sort); do
        clang-format -output-replacements-xml $FN > ./clang-format/xml/$FN.xml
        COUNT=$(xmllint --xpath "count(//replacement)" ./clang-format/xml/$FN.xml)
        if [ $COUNT -gt 0 ]; then
          FAIL=1

          # Show the number replacements suggested by clang-format
          printf "clang-format suggests \e[31m%2d\e[0m changes to: %s\n" $COUNT $FN
        else
          # Remove the XML file if no replacements are suggested
          rm ./clang-format/xml/$FN.xml
        fi
      done

      if [ $FAIL -gt 0 ]; then
        echo
        echo "-------------------------"
        echo -e "\n\e[31mclang-format check failed\e[0m"
        echo "-------------------------"

        # Do In-place edit to all files
        for FN in $(find ./src ./tests -name '*.c' -o -name '*.h'); do
          # In-place edit the files
          clang-format -i $FN
        done

        # Show the differences from the current commit
        git diff -U0 --color=always

        # Fail the job
        exit 1
      else
        echo "-------------------------------------"
        echo -e "\e[32mclang-format is happy with the format\e[0m"
        echo "-------------------------------------"
      fi
  allow_failure: true
  artifacts:
    when: on_failure
    paths:
      - clang-format/xml
    expire_in: "1 week"

# Visualize
visualize:
  extends:
    - .default_code_quality
  stage: visualize
  before_script:
    # Install lcov_cobertura. Use `--break-system-packages` for running on
    # Debian-based distributions.
    - pip install --break-system-packages lcov_cobertura
  script:
    - meson setup ${MESON_ARGS} ${MESON_BUILDDIR}
    - meson test -C ${MESON_BUILDDIR}
    - ninja coverage -C ${MESON_BUILDDIR}
    - ninja coverage-cobertura -C ${MESON_BUILDDIR}
  coverage: '/lines\.+:\s(\d+\.\d+\%)\s+/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: builddir/meson-logs/coverage.xml
    paths:
      - builddir/meson-logs/coverage*
    expire_in: "1 week"
  needs:
    - test-fedora-latest

# Fedora Latest (41)
prep-fedora-latest:
  extends:
    - .fdo.container-build@fedora
    - .default_fedora_variables
  stage: 'prep'
  variables:
    FDO_DISTRIBUTION_VERSION: '41'

test-fedora-latest:
  extends:
    - .fdo.distribution-image@fedora
    - .default_fedora_variables
    - .default_artifacts
    - .default_tests
  variables:
    FDO_DISTRIBUTION_VERSION: '41'
  needs:
    - prep-fedora-latest

# Fedora Rawhide (42)
prep-fedora-rawhide:
  extends:
    - .fdo.container-build@fedora
    - .default_fedora_variables
  stage: 'prep'
  variables:
    FDO_DISTRIBUTION_VERSION: '42'

test-fedora-rawhide:
  extends:
    - .fdo.distribution-image@fedora
    - .default_fedora_variables
    - .default_artifacts
    - .default_tests
  variables:
    FDO_DISTRIBUTION_VERSION: '42'
  needs:
    - prep-fedora-rawhide

# Ubuntu Devel (Plucky)
prep-ubuntu-devel:
  extends:
    - .fdo.container-build@ubuntu
    - .default_ubuntu_variables
    - .default_artifacts
  stage: 'prep'
  variables:
    FDO_DISTRIBUTION_VERSION: '25.04'
  allow_failure: true

test-ubuntu-devel:
  extends:
    - .fdo.distribution-image@ubuntu
    - .default_ubuntu_variables
    - .default_artifacts
    - .default_tests
  variables:
    FDO_DISTRIBUTION_VERSION: '25.04'
  needs:
    - prep-ubuntu-devel
  allow_failure: true

# Ubuntu Latest (Oracular)
prep-ubuntu-latest:
  extends:
    - .fdo.container-build@ubuntu
    - .default_ubuntu_variables
    - .default_artifacts
  stage: 'prep'
  variables:
    FDO_DISTRIBUTION_VERSION: '24.10'

test-ubuntu-latest:
  extends:
    - .fdo.distribution-image@ubuntu
    - .default_ubuntu_variables
    - .default_artifacts
    - .default_tests
  variables:
    FDO_DISTRIBUTION_VERSION: '24.10'
  needs:
    - prep-ubuntu-latest

# Ubuntu LTS (Noble)
prep-ubuntu-lts:
  extends:
    - .fdo.container-build@ubuntu
    - .default_ubuntu_variables
    - .default_artifacts
  stage: 'prep'
  variables:
    FDO_DISTRIBUTION_VERSION: '24.04'

test-ubuntu-lts:
  extends:
    - .fdo.distribution-image@ubuntu
    - .default_ubuntu_variables
    - .default_artifacts
    - .default_tests
  variables:
    FDO_DISTRIBUTION_VERSION: '24.04'
  needs:
    - prep-ubuntu-lts
