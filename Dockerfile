# Base image
FROM fedora:rawhide

RUN dnf -y install \
    gcc gcc-c++ \
    libsqlite3x-devel \
    meson cppcheck && \
    rm -rf /var/cache/dnf /var/log/{dnf*.log,hawkey.log}

# Copy application files
COPY . /app

WORKDIR "/app"

# Build the project
RUN meson setup builddir
RUN meson compile -C builddir
RUN meson install -C builddir

CMD ["/bin/bash"]