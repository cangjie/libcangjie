﻿/* Copyright (c) 2013 - The libcangjie authors.
 *
 * This file is part of libcangjie.
 *
 * libcangjie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libcangjie is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libcangjie.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <cangjie.h>
#include <string.h>

static void test_cangjie_char_a(void) {
    const char *chchar = "a";
    const char *simpchar = "b";
    const char *code = "abc";
    const uint32_t frequency = 123;

    CangjieChar *c;
    int ret = cangjie_char_new(&c, chchar, simpchar, code, frequency);
    assert(ret == CANGJIE_OK);

    assert(strcmp(c->chchar, chchar) == 0);
    assert(strcmp(c->simpchar, simpchar) == 0);
    assert(strcmp(c->code, code) == 0);
    assert(c->frequency == frequency);

    cangjie_char_free(c);
}

static void test_cangjie_char_zh(void) {
    const char *chchar = "\xE5\xBE\x8C";    // 後
    const char *simpchar = "\xE5\x90\x8E";  // 后
    const char *code = "abc";
    uint32_t frequency = 123;

    CangjieChar *c;
    int ret = cangjie_char_new(&c, chchar, simpchar, code, frequency);
    assert(ret == CANGJIE_OK);

    assert(strcmp(c->chchar, chchar) == 0);
    assert(strcmp(c->simpchar, simpchar) == 0);
    assert(strcmp(c->code, code) == 0);
    assert(c->frequency == frequency);

    cangjie_char_free(c);
}

int main(int argc, const char *argv[]) {
    test_cangjie_char_a();
    test_cangjie_char_zh();
    return 0;
}
