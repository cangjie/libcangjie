# libcangjie

[![release-badge]][release-url] [![c-standard-badge]][c-standard-url] [![chat-badge]][chat-url] [![ci-badge]][ci-url]

This is a C library implementing the Cangjie input method.

Below is a trivial example of how to use it:

```c
#include <stdio.h>
#include <cangjie.h>

int main() {
    Cangjie *cj;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3,
                          CANGJIE_FILTER_BIG5 | CANGJIE_FILTER_HKSCS);
    CangjieCharList *chars;
    CangjieCharList *iter;
    ret = cangjie_get_characters(cj, "d*d", &chars);

    if (ret == CANGJIE_NOCHARS) {
        printf("No chars with code '%s'\n", "d*d");
        cangjie_free(cj);
        return 1;
    }

    iter = chars;

    while (1) {
        if (iter == NULL)
            break;

        printf("Char: %s, SimpChar: %s, code: '%s', classic frequency: %d\n",
               iter->c->chchar, iter->c->simpchar, iter->c->code, iter->c->frequency);

        iter = iter->next;
    }

    cangjie_char_list_free(chars);
    cangjie_free(cj);
    return 0;
}
```

## RUN on docker

```bash
$ sudo docker build -t libcangjie .
$ docker run -it libcangjie   
$ cd builddir/src
$ ./libcangjie-cli query hqi
```

### output 

```bash
Char: '我', SimpChar: '我', code: 'hqi', classic frequency: 22308
```

For more details, refer to the documentation, either
[online](https://cangjie.pages.freedesktop.org/projects/libcangjie/documentation)
or the one shipped with this software.

Development happens
[on the Freedesktop Gitlab](https://gitlab.freedesktop.org/cangjie/libcangjie/),
but the stable release tarballs are on
[the releaeses section][release-url] there.

[c-standard-badge]: https://img.shields.io/badge/C-11-blue.svg
[c-standard-url]: https://en.wikipedia.org/wiki/C11_(C_standard_revision)
[chat-badge]: https://img.shields.io/badge/chat-on%20Matrix-blue.svg
[chat-url]: https://app.element.io/#/room/#cangjie:converser.eu
[ci-badge]: https://gitlab.freedesktop.org/cangjie/libcangjie/badges/main/pipeline.svg
[ci-url]: https://gitlab.freedesktop.org/cangjie/libcangjie/-/commits/main
[release-badge]: https://gitlab.freedesktop.org/cangjie/libcangjie/-/badges/release.svg
[release-url]: https://gitlab.freedesktop.org/cangjie/libcangjie/-/releases

## Contributing

Free software can only thrive thanks to people like you who take the
time to help. We appreciate all sorts of help, including but not limited to:

* Coding New Feature or Bug Fixes
* Issue Report
* Testing
* Distribution

Please read our [Contributing docs](CONTRIBUTING.md) for details.

## History

This library is based on
[Wan Leung Wong's original libcangjie](https://github.com/wanleung/libcangjie).

In fact, when writing it, we tried to stay close to the original libcangjie
API, because Wan Leung had done a pretty good job with it.

However, we felt the need to start afresh for a few reasons:

* due to real life constraints, Wan Leung didn't have any time to dedicate to
  libcangjie any more

* we felt that some of the technical decisions in the original libcangjie were
  not the wisest choice

Nevertheless, this library would probably not exist if Wan Leung hadn't opened
the way, so we feel it is important to give him credit.

Thank you very much Wan Leung!

## Legalities

This project is offered under the terms of the
[GNU Lesser General Public License, version 3 or any later version][lgpl], see
the [COPYING](COPYING) file for details.

[lgpl]: http://www.gnu.org/licenses/lgpl.html
