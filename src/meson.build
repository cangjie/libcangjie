api_version = '0.1'

# Support version string of the form "x.y.z[-extra]"
version_str = meson.project_version().split('-')
version_split = version_str[0].split('.')

# Configurations for database location
config_h = configure_file(
  input: 'config.h.in',
  output: 'config.h',
  configuration: global_config,
)
install_headers(config_h, subdir: 'cangjie')

# Build version information into source code
version_conf = configuration_data()
version_conf.set('VERSION', version_str)
version_conf.set('MAJOR_VERSION', version_split[0])
version_conf.set('MINOR_VERSION', version_split[1])
version_conf.set('MICRO_VERSION', version_split[2])
libcangjie_h = configure_file(
  input: 'libcangjie.h.in',
  output: 'libcangjie.h',
  configuration: version_conf,
)
install_headers(libcangjie_h, subdir: 'cangjie')

# Declare header files for build and installation
libcangjie_headers = [
  'cangjie.h',
  'cangjiechar.h',
  'cangjiecharlist.h',
  'cangjiecommon.h',
  'cangjieerrors.h',
]

libcangjie_sources = [
  'cangjie.c',
  'cangjiechar.c',
  'cangjiecharlist.c',
  'libcangjie.c',
]

libcanjie_utils_sources = [
  'cli.c',
  'dbbuilder.c',
  'bench.c',
]

libcangjie_deps = [
  dependency('sqlite3')
]

# Copy the files to the build directory for cppcheck or other tools that
# require the fully assembled source code.
foreach src : libcangjie_headers + libcangjie_sources + libcanjie_utils_sources
  configure_file(
    input: src,
    output: src,
    configuration: global_config,
  )
endforeach

libcangjie_lib = shared_library('cangjie',
  libcangjie_sources,
  dependencies: libcangjie_deps,
       version: cangjie_soname,
       install: true,
)

libcangjie_dep = declare_dependency(
              sources: libcangjie_headers + [libcangjie_h, config_h],
            link_with: libcangjie_lib,
         dependencies: libcangjie_deps,
  include_directories: [include_directories('.')],
)


install_headers(libcangjie_headers, subdir: 'cangjie')

pkg = import('pkgconfig')
pkg.generate(
  description: 'Library implementing the Cangjie input method',
    libraries: libcangjie_lib,
         name: 'libcangjie',
          url: 'https://gitlab.freedesktop.org/cangjie/libcangjie/',
     filebase: 'cangjie',
      subdirs: 'cangjie',
     requires: libcangjie_deps,
)

bench_prog = executable(
  'libcangjie-bench',
  'bench.c',
  dependencies: [libcangjie_dep],
  install: true,
)
meson.override_find_program('libcangjie-bench', bench_prog)

cli_prog = executable(
  'libcangjie-cli',
  'cli.c',
  dependencies: [libcangjie_dep],
  install: true,
)
meson.override_find_program('libcangjie-cli', cli_prog)

dbbuilder_prog = executable(
  'libcangjie-dbbuilder',
  'dbbuilder.c',
  dependencies: [libcangjie_deps],
  install: true,
)
meson.override_find_program('libcangjie-dbbuilder', dbbuilder_prog)
