/* Copyright (c) 2013 - The libcangjie authors.
 *
 * This file is part of libcangjie.
 *
 * libcangjie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libcangjie is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libcangjie.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cangjie.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef _MSC_VER
    #define snprintf sprintf_s
#endif

typedef int __cangjie_get_characters(const Cangjie *, const char *,
                                     CangjieCharList **);

static void iterate_results(CangjieCharList *chars) {
    const CangjieCharList *iter = chars;
    while (1) {
        if (iter == NULL) {
            break;
        }

        iter = iter->next;
    }
}

static void bench_get_characters__one_letter(
    const Cangjie *cj, __cangjie_get_characters *get_characters,
    const char *name) {
    char *code;
    uint32_t count = 0;
    clock_t cStartClock;

    code = malloc(2 * sizeof(char));

    printf("Testing %s: all one-letter codes... ", name);
    cStartClock = clock();
    for (char i = 'a'; i <= 'z'; i++) {
        CangjieCharList *chars = NULL;
        int ret = 0;

        snprintf(code, 2, "%c", i);

        ret = (*get_characters)(cj, code, &chars);

        if (ret != CANGJIE_NOCHARS) {
            iterate_results(chars);

            cangjie_char_list_free(chars);
        }

        count++;
    }
    printf("Ran %u queries in %4.3f milliseconds\n", count,
           1000 * (clock() - cStartClock) / (double)CLOCKS_PER_SEC);

    free(code);
}

static void bench_get_characters__two_letters(
    const Cangjie *cj, __cangjie_get_characters *get_characters,
    const char *name) {
    char *code;
    uint32_t count = 0;
    clock_t cStartClock;
    int ret;

    code = malloc(3 * sizeof(char));

    printf("Testing %s: all two-letters codes... ", name);
    cStartClock = clock();
    for (char i = 'a'; i <= 'z'; i++) {
        for (char j = 'a'; j <= 'z'; j++) {
            CangjieCharList *chars = NULL;

            snprintf(code, 3, "%c%c", i, j);

            ret = (*get_characters)(cj, code, &chars);

            if (ret != CANGJIE_NOCHARS) {
                iterate_results(chars);

                cangjie_char_list_free(chars);
            }

            count++;
        }
    }
    printf("Ran %u queries in %4.3f milliseconds\n", count,
           1000 * (clock() - cStartClock) / (double)CLOCKS_PER_SEC);

    free(code);
}

static void bench_get_characters__three_letters(
    const Cangjie *cj, __cangjie_get_characters *get_characters,
    const char *name) {
    char *code;
    uint32_t count = 0;
    clock_t cStartClock;
    int ret;

    code = malloc(4 * sizeof(char));

    printf("Testing %s: all three-letters codes... ", name);
    cStartClock = clock();
    for (char i = 'a'; i <= 'z'; i++) {
        for (char j = 'a'; j <= 'z'; j++) {
            for (char k = 'a'; k <= 'z'; k++) {
                CangjieCharList *chars = NULL;

                snprintf(code, 4, "%c%c%c", i, j, k);

                ret = (*get_characters)(cj, code, &chars);

                if (ret != CANGJIE_NOCHARS) {
                    iterate_results(chars);

                    cangjie_char_list_free(chars);
                }

                count++;
            }
        }
    }
    printf("Ran %u queries in %4.3f milliseconds\n", count,
           1000 * (clock() - cStartClock) / (double)CLOCKS_PER_SEC);

    free(code);
}

static void bench_get_characters__four_letters(
    const Cangjie *cj, __cangjie_get_characters *get_characters,
    const char *name) {
    char *code;
    uint32_t count = 0;
    clock_t cStartClock;
    int ret;

    code = malloc(5 * sizeof(char));

    printf("Testing %s: all four-letters codes... ", name);
    cStartClock = clock();
    for (char i = 'a'; i <= 'z'; i++) {
        for (char j = 'a'; j <= 'z'; j++) {
            for (char k = 'a'; k <= 'z'; k++) {
                for (char l = 'a'; l <= 'z'; l++) {
                    CangjieCharList *chars = NULL;

                    snprintf(code, 5, "%c%c%c%c", i, j, k, l);

                    ret = (*get_characters)(cj, code, &chars);

                    if (ret != CANGJIE_NOCHARS) {
                        iterate_results(chars);

                        cangjie_char_list_free(chars);
                    }

                    count++;
                }
            }
        }
    }
    printf("Ran %u queries in %4.3f milliseconds\n", count,
           1000 * (clock() - cStartClock) / (double)CLOCKS_PER_SEC);

    free(code);
}

static void bench_get_characters__x_asterisk_y(
    const Cangjie *cj, __cangjie_get_characters *get_characters,
    const char *name) {
    char *code;
    uint32_t count = 0;
    clock_t cStartClock;
    int ret;

    code = malloc(4 * sizeof(char));

    printf("Testing %s: all 'X*Y' codes... ", name);
    cStartClock = clock();
    for (char i = 'a'; i <= 'z'; i++) {
        for (char j = 'a'; j <= 'z'; j++) {
            CangjieCharList *chars = NULL;

            snprintf(code, 4, "%c*%c", i, j);

            ret = (*get_characters)(cj, code, &chars);

            if (ret != CANGJIE_NOCHARS) {
                iterate_results(chars);

                cangjie_char_list_free(chars);
            }

            count++;
        }
    }
    printf("Ran %u queries in %4.3f milliseconds\n", count,
           1000 * (clock() - cStartClock) / (double)CLOCKS_PER_SEC);

    free(code);
}

int main(int argc, char **argv) {
    Cangjie *cj;

    int ret = cangjie_new(&cj, CANGJIE_VERSION_3,
                          CANGJIE_FILTER_BIG5 | CANGJIE_FILTER_HKSCS);
    if (ret == CANGJIE_DBOPEN) {
        printf("Could not open the Cangjie database\n");
        return ret;
    } else if (ret != CANGJIE_OK) {
        printf("Unhandled error while creating the Cangjie object: %d\n", ret);
        return ret;
    }

    bench_get_characters__one_letter(cj, cangjie_get_characters,
                                     "cangjie_get_characters");
    bench_get_characters__two_letters(cj, cangjie_get_characters,
                                      "cangjie_get_characters");
    bench_get_characters__three_letters(cj, cangjie_get_characters,
                                        "cangjie_get_characters");
    bench_get_characters__four_letters(cj, cangjie_get_characters,
                                       "cangjie_get_characters");
    bench_get_characters__x_asterisk_y(cj, cangjie_get_characters,
                                       "cangjie_get_characters");
    bench_get_characters__one_letter(cj, cangjie_get_characters_v2,
                                     "cangjie_get_characters_v2");
    bench_get_characters__two_letters(cj, cangjie_get_characters_v2,
                                      "cangjie_get_characters_v2");
    bench_get_characters__three_letters(cj, cangjie_get_characters_v2,
                                        "cangjie_get_characters_v2");
    bench_get_characters__four_letters(cj, cangjie_get_characters_v2,
                                       "cangjie_get_characters_v2");
    bench_get_characters__x_asterisk_y(cj, cangjie_get_characters_v2,
                                       "cangjie_get_characters_v2");

    cangjie_free(cj);
    return CANGJIE_OK;
}
